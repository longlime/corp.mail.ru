$(document).ready(function(){
	$(".select-name").click(function(){
		var display = $(this).next(".select-list").css("display");
		$(".select-list").hide();
		if(display=='none') $(this).next(".select-list").show();
	});
	$(".calculator-select").click(function(){
		var display = $(this).next(".calculator-select-list").css("display");
		$(".calculator-select-list").hide();
		if(display=='none') $(this).next(".calculator-select-list").show();
	});

	new ((function() {
		return function() {
			var goTo = function(sel) {
				var $block = $(sel);
				if ($block.size()) {
					var offsetTop = $block.offset().top;
					if ($(".header-menu").size()) offsetTop -= 62;
					$("html, body").animate({scrollTop: offsetTop});
				}
			};

			$("a[data-anchor]").on("click", function() {
				goTo($(this).data("anchor"));
				return false;
			});
		};
	})());


	$(".tab").click(function(){
		$(".tabs-handlers a").removeClass("_active");
		$(this).addClass("_active");
		$(".tab-content").removeClass("tab-active");

		var tab = $(this).attr("tab");

		$(".content").each(function(){
			if($(this).attr("data-tab")==tab){
				$(this).addClass("tab-active");
				return;
			}
		});

	});

	$(".tab_title").click(function(){
		$(".tab_title").removeClass("tab_title_active");
		$(this).addClass("tab_title_active");
		$(".tab-content").removeClass("tab-active");

		var tab = $(this).attr("tab");

		$(".content").each(function(){
			if($(this).attr("data-tab")==tab){
				$(this).addClass("tab-active");
				return;
			}
		});

	});
	$(".small-tab").click(function(){
		$(".small-tab").removeClass("_active-small-tab");
		$(this).addClass("_active-small-tab");
		$(".small-tab-content").removeClass("active-content");

		var tab = $(this).attr("tab");

		$(".small-tab-content").each(function(){
			if($(this).attr("data-tab")==tab)
			{
				$(this).addClass("active-content");
				return;
			}
		});
	});

	$(document).scroll(function(){
		var $b_float = $("#float");
		if ($b_float) return;
		var left=$b_float.offset().left;
		var off = $b_float.attr("offset");
		if(($(this).scrollTop()>=200)){
			if($(this).scrollTop()<=off){
				$b_float.removeClass("fixed-1");
				$b_float.addClass("fixed");
				$b_float.css("left",left+"px");
				$b_float.css("top","80px");
			}else{
				$b_float.removeClass("fixed");
				$b_float.addClass("fixed-1");
				$b_float.css("left",left+"px");
				$b_float.css("top",off+"px");
			}
		}else{
			$b_float.removeClass("fixed");
			$b_float.removeClass("fixed-1");
			$b_float.css("left","0px");
		}
	});

	// Fucking css :HOVER effect
	var cssHOVER = "#0256A4";
	$(document).find("a").hover(
		function(){
			if($(this).attr("do")!='no'){
				$(this).css("color",cssHOVER);
			}
		},function(){
			if($(this).attr("do")!='no'){
				$(this).css("color","");
			}
		});

	// Fucking css :HOVER effect for eventlist
	// Because link inside link doesn't work :(
	$("._press-events").find(".eventslist li").find("> a:eq(0), > .event-title").hover(
		function() {
			$(this).parent().addClass("_hovered");
		}, function() {
			$(this).parent().removeClass("_hovered");
		});

	$(".directions-block").attr("style","");

	var HeaderMenu = (function() {
		var
			showClass = "_showed",
			dur = 300,
			$header = $("#header"),
			$headerMenu = $(".header-menu").not(".first-active-menu"),
			showByPoint = function(point) {
				var block = $("#submenu-" + point);
				if (!block.hasClass("first-active-menu")) {
					$header.addClass("-active");
					block.stop().fadeIn(dur);
					$(".header-navigation-list li[submenu=" + point + "]").addClass("header-menu-hover");
				}
			},
			show = function() {
				var point = $(this).attr("submenu") || $(this).attr("menu");
				showByPoint(point);
			},
			hide = function() {
				$header.removeClass("-active");
				$headerMenu.stop().fadeOut(dur);
				$header.find(".header-menu-hover").removeClass("header-menu-hover");
			};

		return function() {
			$(".header-navigation-list li[submenu]").hover(show, hide);
			$headerMenu.hover(show, hide);
		};
	})();

	var YearsControlers = (function() {
		var $yearsList = $("#years-list");
		var $yearMain = $("#main-year");
		var $block = $("#history-map > .years-history");
		var $window = $(window);

		var select = function(i) {
			var px = (-182)*i;
			$yearsList.animate({left:px+"px"},500);
			$yearsList.find("li").removeClass("active-year").eq(i).addClass("active-year");
			$yearMain.find("span").html($yearsList.find("li").eq(i).attr("id"));
			$yearMain.attr("year", $yearsList.find("li").eq(i).attr("id"));
		};

		var move = function(index) {
			var sum = 0;
			$block.find(".year-history").each(function(i, item) {
				if (i === index) {return false;}
				sum += $(item).width();
			});

			$block.animate({left: -sum}, {
				duration: 500,
				step: function() {
					setBlur( $block.offset().left );
				}
			});
		};

		var blurBlock = $(".history-blur");
		var currentBlurStyle = blurBlock.attr("style");
		var setBlur = function(value) {
			// console.log( blurBlock.find(".years-history") );
			// console.log( (currentBlurStyle||"") + " -webkit-transform: translate3d(" + value + "px, 0, 0);" );
			blurBlock.attr("style", (currentBlurStyle||"") + " -webkit-transform: translate3d(" + value + "px, 0, 0);");
		};

		return function() {
			var index = 0;
			var lastIndex = index;

			// console.log( $block.find(".year-history").css("marginRight") );
			// $block.width($block.find(".year-history").size() * $block.find(".year-history").width());
			$block.draggable({ axis: "x",
				drag: function() {
					var current = -$block.offset().left + $window.width();
					// console.log( current );
					var sum = 0;
					$block.find(".year-history").each(function(i, item) {
						sum += $(item).width();
						index = i;
						if (current < sum) {return false;}
					});

					if (index !== lastIndex) {
						select(index-1<0?0:index-1);
						lastIndex = index;
					}

					setBlur( $block.offset().left );
				}
			});

			$block.find(".year-history").width(1450);
			var sumWidth = 1450 * 5;
			$block.width(sumWidth);
			// console.log( $block.find(".year-history").css("paddingLeft") );

			var minLeft = - (sumWidth - $window.width());

			$window.mousemove(function(){
				if ($block.offset()){
					if($block.offset().left>0){
						$block.offset({left: 0});
					} else if ($block.offset().left<minLeft) {
						$block.offset({left: minLeft});
					}
				}
			});

			$yearsList.find("a").bind("click", function() {
				var index = $(this).parent().index();
				select(index);
				move(index);
			});

			$("#year-prev").bind("click", function() {
				var id = $(".active-year").attr("prev");
				if (id) {
					var index = $(".year").filter("#" + id).index();
					select(index);
					move(index);
				}
			});
			$("#year-next").bind("click", function() {
				var id = $(".active-year").attr("next");
				if (id && $("#"+id).attr("disabled")==undefined) {
					var index = $(".year").filter("#" + id).index();
					select(index);
					move(index);
				}
			});

			new ((function() {
				return function() {
					var block = $("#history-points");
					var content = "<div class='history-blur-content'>" + $(".years-history").clone().wrap("<div />").parent().html() + "</div>";
					block
						.parent()
						.append("<div class='history-blur'>" + content + "</div>");
					// console.log( "a" );
				};
			})());

			blurBlock = $(".history-blur").find(".years-history");
			currentBlurStyle = blurBlock.attr("style");
		};
	})();

	$(".header-menu-content li").hover(
		function(){
			var subtext=$(this).attr("subtext");
			$(this).parent().parent().find("#subtext-"+subtext).addClass("subtext-show");
		},function(){
			var subtext=$(this).attr("subtext");
			$(this).parent().parent().find("#subtext-"+subtext).removeClass("subtext-show");
		});
	$(".subtext").hover(function(){$(this).addClass("subtext-show");},function(){$(this).removeClass("subtext-show");});


	var MapTrigger = (function() {
		function app() {
			var $offices = $(".office-city");
			var map = $(".map-trigger-position");

			if (map.size()) {

				var mapPos = map.offset().top - 62;

				$offices.on("click", "a", function() {
					$("html, body").animate({scrollTop: mapPos}, 300);
					map.attr("src", $(this).parent().data("map"));
					return false;
				});

			}
		};

		return app;
	})();

	new HeaderMenu();
	new MapTrigger();

	/*! jQuery UI - v1.10.3 - 2013-12-11
	* http://jqueryui.com
	* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.draggable.js
	* Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */

	(function(e,t){function i(t,i){var s,n,r,o=t.nodeName.toLowerCase();return"area"===o?(s=t.parentNode,n=s.name,t.href&&n&&"map"===s.nodeName.toLowerCase()?(r=e("img[usemap=#"+n+"]")[0],!!r&&a(r)):!1):(/input|select|textarea|button|object/.test(o)?!t.disabled:"a"===o?t.href||i:i)&&a(t)}function a(t){return e.expr.filters.visible(t)&&!e(t).parents().addBack().filter(function(){return"hidden"===e.css(this,"visibility")}).length}var s=0,n=/^ui-id-\d+$/;e.ui=e.ui||{},e.extend(e.ui,{version:"1.10.3",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),e.fn.extend({focus:function(t){return function(i,a){return"number"==typeof i?this.each(function(){var t=this;setTimeout(function(){e(t).focus(),a&&a.call(t)},i)}):t.apply(this,arguments)}}(e.fn.focus),scrollParent:function(){var t;return t=e.ui.ie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?this.parents().filter(function(){return/(relative|absolute|fixed)/.test(e.css(this,"position"))&&/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0):this.parents().filter(function(){return/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0),/fixed/.test(this.css("position"))||!t.length?e(document):t},zIndex:function(i){if(i!==t)return this.css("zIndex",i);if(this.length)for(var a,s,n=e(this[0]);n.length&&n[0]!==document;){if(a=n.css("position"),("absolute"===a||"relative"===a||"fixed"===a)&&(s=parseInt(n.css("zIndex"),10),!isNaN(s)&&0!==s))return s;n=n.parent()}return 0},uniqueId:function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++s)})},removeUniqueId:function(){return this.each(function(){n.test(this.id)&&e(this).removeAttr("id")})}}),e.extend(e.expr[":"],{data:e.expr.createPseudo?e.expr.createPseudo(function(t){return function(i){return!!e.data(i,t)}}):function(t,i,a){return!!e.data(t,a[3])},focusable:function(t){return i(t,!isNaN(e.attr(t,"tabindex")))},tabbable:function(t){var a=e.attr(t,"tabindex"),s=isNaN(a);return(s||a>=0)&&i(t,!s)}}),e("<a>").outerWidth(1).jquery||e.each(["Width","Height"],function(i,a){function s(t,i,a,s){return e.each(n,function(){i-=parseFloat(e.css(t,"padding"+this))||0,a&&(i-=parseFloat(e.css(t,"border"+this+"Width"))||0),s&&(i-=parseFloat(e.css(t,"margin"+this))||0)}),i}var n="Width"===a?["Left","Right"]:["Top","Bottom"],r=a.toLowerCase(),o={innerWidth:e.fn.innerWidth,innerHeight:e.fn.innerHeight,outerWidth:e.fn.outerWidth,outerHeight:e.fn.outerHeight};e.fn["inner"+a]=function(i){return i===t?o["inner"+a].call(this):this.each(function(){e(this).css(r,s(this,i)+"px")})},e.fn["outer"+a]=function(t,i){return"number"!=typeof t?o["outer"+a].call(this,t):this.each(function(){e(this).css(r,s(this,t,!0,i)+"px")})}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}),e("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(e.fn.removeData=function(t){return function(i){return arguments.length?t.call(this,e.camelCase(i)):t.call(this)}}(e.fn.removeData)),e.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),e.support.selectstart="onselectstart"in document.createElement("div"),e.fn.extend({disableSelection:function(){return this.bind((e.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(e){e.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}}),e.extend(e.ui,{plugin:{add:function(t,i,a){var s,n=e.ui[t].prototype;for(s in a)n.plugins[s]=n.plugins[s]||[],n.plugins[s].push([i,a[s]])},call:function(e,t,i){var a,s=e.plugins[t];if(s&&e.element[0].parentNode&&11!==e.element[0].parentNode.nodeType)for(a=0;s.length>a;a++)e.options[s[a][0]]&&s[a][1].apply(e.element,i)}},hasScroll:function(t,i){if("hidden"===e(t).css("overflow"))return!1;var a=i&&"left"===i?"scrollLeft":"scrollTop",s=!1;return t[a]>0?!0:(t[a]=1,s=t[a]>0,t[a]=0,s)}})})(jQuery);(function(e,t){var i=0,s=Array.prototype.slice,a=e.cleanData;e.cleanData=function(t){for(var i,s=0;null!=(i=t[s]);s++)try{e(i).triggerHandler("remove")}catch(n){}a(t)},e.widget=function(i,s,a){var n,r,o,h,l={},u=i.split(".")[0];i=i.split(".")[1],n=u+"-"+i,a||(a=s,s=e.Widget),e.expr[":"][n.toLowerCase()]=function(t){return!!e.data(t,n)},e[u]=e[u]||{},r=e[u][i],o=e[u][i]=function(e,i){return this._createWidget?(arguments.length&&this._createWidget(e,i),t):new o(e,i)},e.extend(o,r,{version:a.version,_proto:e.extend({},a),_childConstructors:[]}),h=new s,h.options=e.widget.extend({},h.options),e.each(a,function(i,a){return e.isFunction(a)?(l[i]=function(){var e=function(){return s.prototype[i].apply(this,arguments)},t=function(e){return s.prototype[i].apply(this,e)};return function(){var i,s=this._super,n=this._superApply;return this._super=e,this._superApply=t,i=a.apply(this,arguments),this._super=s,this._superApply=n,i}}(),t):(l[i]=a,t)}),o.prototype=e.widget.extend(h,{widgetEventPrefix:r?h.widgetEventPrefix:i},l,{constructor:o,namespace:u,widgetName:i,widgetFullName:n}),r?(e.each(r._childConstructors,function(t,i){var s=i.prototype;e.widget(s.namespace+"."+s.widgetName,o,i._proto)}),delete r._childConstructors):s._childConstructors.push(o),e.widget.bridge(i,o)},e.widget.extend=function(i){for(var a,n,r=s.call(arguments,1),o=0,h=r.length;h>o;o++)for(a in r[o])n=r[o][a],r[o].hasOwnProperty(a)&&n!==t&&(i[a]=e.isPlainObject(n)?e.isPlainObject(i[a])?e.widget.extend({},i[a],n):e.widget.extend({},n):n);return i},e.widget.bridge=function(i,a){var n=a.prototype.widgetFullName||i;e.fn[i]=function(r){var o="string"==typeof r,h=s.call(arguments,1),l=this;return r=!o&&h.length?e.widget.extend.apply(null,[r].concat(h)):r,o?this.each(function(){var s,a=e.data(this,n);return a?e.isFunction(a[r])&&"_"!==r.charAt(0)?(s=a[r].apply(a,h),s!==a&&s!==t?(l=s&&s.jquery?l.pushStack(s.get()):s,!1):t):e.error("no such method '"+r+"' for "+i+" widget instance"):e.error("cannot call methods on "+i+" prior to initialization; "+"attempted to call method '"+r+"'")}):this.each(function(){var t=e.data(this,n);t?t.option(r||{})._init():e.data(this,n,new a(r,this))}),l}},e.Widget=function(){},e.Widget._childConstructors=[],e.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(t,s){s=e(s||this.defaultElement||this)[0],this.element=e(s),this.uuid=i++,this.eventNamespace="."+this.widgetName+this.uuid,this.options=e.widget.extend({},this.options,this._getCreateOptions(),t),this.bindings=e(),this.hoverable=e(),this.focusable=e(),s!==this&&(e.data(s,this.widgetFullName,this),this._on(!0,this.element,{remove:function(e){e.target===s&&this.destroy()}}),this.document=e(s.style?s.ownerDocument:s.document||s),this.window=e(this.document[0].defaultView||this.document[0].parentWindow)),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:e.noop,_getCreateEventData:e.noop,_create:e.noop,_init:e.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:e.noop,widget:function(){return this.element},option:function(i,s){var a,n,r,o=i;if(0===arguments.length)return e.widget.extend({},this.options);if("string"==typeof i)if(o={},a=i.split("."),i=a.shift(),a.length){for(n=o[i]=e.widget.extend({},this.options[i]),r=0;a.length-1>r;r++)n[a[r]]=n[a[r]]||{},n=n[a[r]];if(i=a.pop(),s===t)return n[i]===t?null:n[i];n[i]=s}else{if(s===t)return this.options[i]===t?null:this.options[i];o[i]=s}return this._setOptions(o),this},_setOptions:function(e){var t;for(t in e)this._setOption(t,e[t]);return this},_setOption:function(e,t){return this.options[e]=t,"disabled"===e&&(this.widget().toggleClass(this.widgetFullName+"-disabled ui-state-disabled",!!t).attr("aria-disabled",t),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")),this},enable:function(){return this._setOption("disabled",!1)},disable:function(){return this._setOption("disabled",!0)},_on:function(i,s,a){var n,r=this;"boolean"!=typeof i&&(a=s,s=i,i=!1),a?(s=n=e(s),this.bindings=this.bindings.add(s)):(a=s,s=this.element,n=this.widget()),e.each(a,function(a,o){function h(){return i||r.options.disabled!==!0&&!e(this).hasClass("ui-state-disabled")?("string"==typeof o?r[o]:o).apply(r,arguments):t}"string"!=typeof o&&(h.guid=o.guid=o.guid||h.guid||e.guid++);var l=a.match(/^(\w+)\s*(.*)$/),u=l[1]+r.eventNamespace,c=l[2];c?n.delegate(c,u,h):s.bind(u,h)})},_off:function(e,t){t=(t||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,e.unbind(t).undelegate(t)},_delay:function(e,t){function i(){return("string"==typeof e?s[e]:e).apply(s,arguments)}var s=this;return setTimeout(i,t||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){e(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){e(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){e(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){e(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,i,s){var a,n,r=this.options[t];if(s=s||{},i=e.Event(i),i.type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),i.target=this.element[0],n=i.originalEvent)for(a in n)a in i||(i[a]=n[a]);return this.element.trigger(i,s),!(e.isFunction(r)&&r.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},e.each({show:"fadeIn",hide:"fadeOut"},function(t,i){e.Widget.prototype["_"+t]=function(s,a,n){"string"==typeof a&&(a={effect:a});var r,o=a?a===!0||"number"==typeof a?i:a.effect||i:t;a=a||{},"number"==typeof a&&(a={duration:a}),r=!e.isEmptyObject(a),a.complete=n,a.delay&&s.delay(a.delay),r&&e.effects&&e.effects.effect[o]?s[t](a):o!==t&&s[o]?s[o](a.duration,a.easing,n):s.queue(function(i){e(this)[t](),n&&n.call(s[0]),i()})}})})(jQuery);(function(e){var t=!1;e(document).mouseup(function(){t=!1}),e.widget("ui.mouse",{version:"1.10.3",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var t=this;this.element.bind("mousedown."+this.widgetName,function(e){return t._mouseDown(e)}).bind("click."+this.widgetName,function(i){return!0===e.data(i.target,t.widgetName+".preventClickEvent")?(e.removeData(i.target,t.widgetName+".preventClickEvent"),i.stopImmediatePropagation(),!1):undefined}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName),this._mouseMoveDelegate&&e(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(i){if(!t){this._mouseStarted&&this._mouseUp(i),this._mouseDownEvent=i;var s=this,a=1===i.which,n="string"==typeof this.options.cancel&&i.target.nodeName?e(i.target).closest(this.options.cancel).length:!1;return a&&!n&&this._mouseCapture(i)?(this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){s.mouseDelayMet=!0},this.options.delay)),this._mouseDistanceMet(i)&&this._mouseDelayMet(i)&&(this._mouseStarted=this._mouseStart(i)!==!1,!this._mouseStarted)?(i.preventDefault(),!0):(!0===e.data(i.target,this.widgetName+".preventClickEvent")&&e.removeData(i.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(e){return s._mouseMove(e)},this._mouseUpDelegate=function(e){return s._mouseUp(e)},e(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),i.preventDefault(),t=!0,!0)):!0}},_mouseMove:function(t){return e.ui.ie&&(!document.documentMode||9>document.documentMode)&&!t.button?this._mouseUp(t):this._mouseStarted?(this._mouseDrag(t),t.preventDefault()):(this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,t)!==!1,this._mouseStarted?this._mouseDrag(t):this._mouseUp(t)),!this._mouseStarted)},_mouseUp:function(t){return e(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,t.target===this._mouseDownEvent.target&&e.data(t.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(t)),!1},_mouseDistanceMet:function(e){return Math.max(Math.abs(this._mouseDownEvent.pageX-e.pageX),Math.abs(this._mouseDownEvent.pageY-e.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}})})(jQuery);(function(e){e.widget("ui.draggable",e.ui.mouse,{version:"1.10.3",widgetEventPrefix:"drag",options:{addClasses:!0,appendTo:"parent",axis:!1,connectToSortable:!1,containment:!1,cursor:"auto",cursorAt:!1,grid:!1,handle:!1,helper:"original",iframeFix:!1,opacity:!1,refreshPositions:!1,revert:!1,revertDuration:500,scope:"default",scroll:!0,scrollSensitivity:20,scrollSpeed:20,snap:!1,snapMode:"both",snapTolerance:20,stack:!1,zIndex:!1,drag:null,start:null,stop:null},_create:function(){"original"!==this.options.helper||/^(?:r|a|f)/.test(this.element.css("position"))||(this.element[0].style.position="relative"),this.options.addClasses&&this.element.addClass("ui-draggable"),this.options.disabled&&this.element.addClass("ui-draggable-disabled"),this._mouseInit()},_destroy:function(){this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"),this._mouseDestroy()},_mouseCapture:function(t){var i=this.options;return this.helper||i.disabled||e(t.target).closest(".ui-resizable-handle").length>0?!1:(this.handle=this._getHandle(t),this.handle?(e(i.iframeFix===!0?"iframe":i.iframeFix).each(function(){e("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({width:this.offsetWidth+"px",height:this.offsetHeight+"px",position:"absolute",opacity:"0.001",zIndex:1e3}).css(e(this).offset()).appendTo("body")}),!0):!1)},_mouseStart:function(t){var i=this.options;return this.helper=this._createHelper(t),this.helper.addClass("ui-draggable-dragging"),this._cacheHelperProportions(),e.ui.ddmanager&&(e.ui.ddmanager.current=this),this._cacheMargins(),this.cssPosition=this.helper.css("position"),this.scrollParent=this.helper.scrollParent(),this.offsetParent=this.helper.offsetParent(),this.offsetParentCssPosition=this.offsetParent.css("position"),this.offset=this.positionAbs=this.element.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},this.offset.scroll=!1,e.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.originalPosition=this.position=this._generatePosition(t),this.originalPageX=t.pageX,this.originalPageY=t.pageY,i.cursorAt&&this._adjustOffsetFromHelper(i.cursorAt),this._setContainment(),this._trigger("start",t)===!1?(this._clear(),!1):(this._cacheHelperProportions(),e.ui.ddmanager&&!i.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this._mouseDrag(t,!0),e.ui.ddmanager&&e.ui.ddmanager.dragStart(this,t),!0)},_mouseDrag:function(t,i){if("fixed"===this.offsetParentCssPosition&&(this.offset.parent=this._getParentOffset()),this.position=this._generatePosition(t),this.positionAbs=this._convertPositionTo("absolute"),!i){var a=this._uiHash();if(this._trigger("drag",t,a)===!1)return this._mouseUp({}),!1;this.position=a.position}return this.options.axis&&"y"===this.options.axis||(this.helper[0].style.left=this.position.left+"px"),this.options.axis&&"x"===this.options.axis||(this.helper[0].style.top=this.position.top+"px"),e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),!1},_mouseStop:function(t){var i=this,a=!1;return e.ui.ddmanager&&!this.options.dropBehaviour&&(a=e.ui.ddmanager.drop(this,t)),this.dropped&&(a=this.dropped,this.dropped=!1),"original"!==this.options.helper||e.contains(this.element[0].ownerDocument,this.element[0])?("invalid"===this.options.revert&&!a||"valid"===this.options.revert&&a||this.options.revert===!0||e.isFunction(this.options.revert)&&this.options.revert.call(this.element,a)?e(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){i._trigger("stop",t)!==!1&&i._clear()}):this._trigger("stop",t)!==!1&&this._clear(),!1):!1},_mouseUp:function(t){return e("div.ui-draggable-iframeFix").each(function(){this.parentNode.removeChild(this)}),e.ui.ddmanager&&e.ui.ddmanager.dragStop(this,t),e.ui.mouse.prototype._mouseUp.call(this,t)},cancel:function(){return this.helper.is(".ui-draggable-dragging")?this._mouseUp({}):this._clear(),this},_getHandle:function(t){return this.options.handle?!!e(t.target).closest(this.element.find(this.options.handle)).length:!0},_createHelper:function(t){var i=this.options,a=e.isFunction(i.helper)?e(i.helper.apply(this.element[0],[t])):"clone"===i.helper?this.element.clone().removeAttr("id"):this.element;return a.parents("body").length||a.appendTo("parent"===i.appendTo?this.element[0].parentNode:i.appendTo),a[0]===this.element[0]||/(fixed|absolute)/.test(a.css("position"))||a.css("position","absolute"),a},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_getParentOffset:function(){var t=this.offsetParent.offset();return"absolute"===this.cssPosition&&this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&"html"===this.offsetParent[0].tagName.toLowerCase()&&e.ui.ie)&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"===this.cssPosition){var e=this.element.position();return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:e.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0,right:parseInt(this.element.css("marginRight"),10)||0,bottom:parseInt(this.element.css("marginBottom"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,i,a,s=this.options;return s.containment?"window"===s.containment?(this.containment=[e(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,e(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,e(window).scrollLeft()+e(window).width()-this.helperProportions.width-this.margins.left,e(window).scrollTop()+(e(window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top],undefined):"document"===s.containment?(this.containment=[0,0,e(document).width()-this.helperProportions.width-this.margins.left,(e(document).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top],undefined):s.containment.constructor===Array?(this.containment=s.containment,undefined):("parent"===s.containment&&(s.containment=this.helper[0].parentNode),i=e(s.containment),a=i[0],a&&(t="hidden"!==i.css("overflow"),this.containment=[(parseInt(i.css("borderLeftWidth"),10)||0)+(parseInt(i.css("paddingLeft"),10)||0),(parseInt(i.css("borderTopWidth"),10)||0)+(parseInt(i.css("paddingTop"),10)||0),(t?Math.max(a.scrollWidth,a.offsetWidth):a.offsetWidth)-(parseInt(i.css("borderRightWidth"),10)||0)-(parseInt(i.css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(t?Math.max(a.scrollHeight,a.offsetHeight):a.offsetHeight)-(parseInt(i.css("borderBottomWidth"),10)||0)-(parseInt(i.css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom],this.relative_container=i),undefined):(this.containment=null,undefined)},_convertPositionTo:function(t,i){i||(i=this.position);var a="absolute"===t?1:-1,s="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent;return this.offset.scroll||(this.offset.scroll={top:s.scrollTop(),left:s.scrollLeft()}),{top:i.top+this.offset.relative.top*a+this.offset.parent.top*a-("fixed"===this.cssPosition?-this.scrollParent.scrollTop():this.offset.scroll.top)*a,left:i.left+this.offset.relative.left*a+this.offset.parent.left*a-("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():this.offset.scroll.left)*a}},_generatePosition:function(t){var i,a,s,n,r=this.options,o="absolute"!==this.cssPosition||this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,l=t.pageX,h=t.pageY;return this.offset.scroll||(this.offset.scroll={top:o.scrollTop(),left:o.scrollLeft()}),this.originalPosition&&(this.containment&&(this.relative_container?(a=this.relative_container.offset(),i=[this.containment[0]+a.left,this.containment[1]+a.top,this.containment[2]+a.left,this.containment[3]+a.top]):i=this.containment,t.pageX-this.offset.click.left<i[0]&&(l=i[0]+this.offset.click.left),t.pageY-this.offset.click.top<i[1]&&(h=i[1]+this.offset.click.top),t.pageX-this.offset.click.left>i[2]&&(l=i[2]+this.offset.click.left),t.pageY-this.offset.click.top>i[3]&&(h=i[3]+this.offset.click.top)),r.grid&&(s=r.grid[1]?this.originalPageY+Math.round((h-this.originalPageY)/r.grid[1])*r.grid[1]:this.originalPageY,h=i?s-this.offset.click.top>=i[1]||s-this.offset.click.top>i[3]?s:s-this.offset.click.top>=i[1]?s-r.grid[1]:s+r.grid[1]:s,n=r.grid[0]?this.originalPageX+Math.round((l-this.originalPageX)/r.grid[0])*r.grid[0]:this.originalPageX,l=i?n-this.offset.click.left>=i[0]||n-this.offset.click.left>i[2]?n:n-this.offset.click.left>=i[0]?n-r.grid[0]:n+r.grid[0]:n)),{top:h-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.scrollParent.scrollTop():this.offset.scroll.top),left:l-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.scrollParent.scrollLeft():this.offset.scroll.left)}},_clear:function(){this.helper.removeClass("ui-draggable-dragging"),this.helper[0]===this.element[0]||this.cancelHelperRemoval||this.helper.remove(),this.helper=null,this.cancelHelperRemoval=!1},_trigger:function(t,i,a){return a=a||this._uiHash(),e.ui.plugin.call(this,t,[i,a]),"drag"===t&&(this.positionAbs=this._convertPositionTo("absolute")),e.Widget.prototype._trigger.call(this,t,i,a)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}}),e.ui.plugin.add("draggable","connectToSortable",{start:function(t,i){var a=e(this).data("ui-draggable"),s=a.options,n=e.extend({},i,{item:a.element});a.sortables=[],e(s.connectToSortable).each(function(){var i=e.data(this,"ui-sortable");i&&!i.options.disabled&&(a.sortables.push({instance:i,shouldRevert:i.options.revert}),i.refreshPositions(),i._trigger("activate",t,n))})},stop:function(t,i){var a=e(this).data("ui-draggable"),s=e.extend({},i,{item:a.element});e.each(a.sortables,function(){this.instance.isOver?(this.instance.isOver=0,a.cancelHelperRemoval=!0,this.instance.cancelHelperRemoval=!1,this.shouldRevert&&(this.instance.options.revert=this.shouldRevert),this.instance._mouseStop(t),this.instance.options.helper=this.instance.options._helper,"original"===a.options.helper&&this.instance.currentItem.css({top:"auto",left:"auto"})):(this.instance.cancelHelperRemoval=!1,this.instance._trigger("deactivate",t,s))})},drag:function(t,i){var a=e(this).data("ui-draggable"),s=this;e.each(a.sortables,function(){var n=!1,r=this;this.instance.positionAbs=a.positionAbs,this.instance.helperProportions=a.helperProportions,this.instance.offset.click=a.offset.click,this.instance._intersectsWith(this.instance.containerCache)&&(n=!0,e.each(a.sortables,function(){return this.instance.positionAbs=a.positionAbs,this.instance.helperProportions=a.helperProportions,this.instance.offset.click=a.offset.click,this!==r&&this.instance._intersectsWith(this.instance.containerCache)&&e.contains(r.instance.element[0],this.instance.element[0])&&(n=!1),n})),n?(this.instance.isOver||(this.instance.isOver=1,this.instance.currentItem=e(s).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item",!0),this.instance.options._helper=this.instance.options.helper,this.instance.options.helper=function(){return i.helper[0]},t.target=this.instance.currentItem[0],this.instance._mouseCapture(t,!0),this.instance._mouseStart(t,!0,!0),this.instance.offset.click.top=a.offset.click.top,this.instance.offset.click.left=a.offset.click.left,this.instance.offset.parent.left-=a.offset.parent.left-this.instance.offset.parent.left,this.instance.offset.parent.top-=a.offset.parent.top-this.instance.offset.parent.top,a._trigger("toSortable",t),a.dropped=this.instance.element,a.currentItem=a.element,this.instance.fromOutside=a),this.instance.currentItem&&this.instance._mouseDrag(t)):this.instance.isOver&&(this.instance.isOver=0,this.instance.cancelHelperRemoval=!0,this.instance.options.revert=!1,this.instance._trigger("out",t,this.instance._uiHash(this.instance)),this.instance._mouseStop(t,!0),this.instance.options.helper=this.instance.options._helper,this.instance.currentItem.remove(),this.instance.placeholder&&this.instance.placeholder.remove(),a._trigger("fromSortable",t),a.dropped=!1)})}}),e.ui.plugin.add("draggable","cursor",{start:function(){var t=e("body"),i=e(this).data("ui-draggable").options;t.css("cursor")&&(i._cursor=t.css("cursor")),t.css("cursor",i.cursor)},stop:function(){var t=e(this).data("ui-draggable").options;t._cursor&&e("body").css("cursor",t._cursor)}}),e.ui.plugin.add("draggable","opacity",{start:function(t,i){var a=e(i.helper),s=e(this).data("ui-draggable").options;a.css("opacity")&&(s._opacity=a.css("opacity")),a.css("opacity",s.opacity)},stop:function(t,i){var a=e(this).data("ui-draggable").options;a._opacity&&e(i.helper).css("opacity",a._opacity)}}),e.ui.plugin.add("draggable","scroll",{start:function(){var t=e(this).data("ui-draggable");t.scrollParent[0]!==document&&"HTML"!==t.scrollParent[0].tagName&&(t.overflowOffset=t.scrollParent.offset())},drag:function(t){var i=e(this).data("ui-draggable"),a=i.options,s=!1;i.scrollParent[0]!==document&&"HTML"!==i.scrollParent[0].tagName?(a.axis&&"x"===a.axis||(i.overflowOffset.top+i.scrollParent[0].offsetHeight-t.pageY<a.scrollSensitivity?i.scrollParent[0].scrollTop=s=i.scrollParent[0].scrollTop+a.scrollSpeed:t.pageY-i.overflowOffset.top<a.scrollSensitivity&&(i.scrollParent[0].scrollTop=s=i.scrollParent[0].scrollTop-a.scrollSpeed)),a.axis&&"y"===a.axis||(i.overflowOffset.left+i.scrollParent[0].offsetWidth-t.pageX<a.scrollSensitivity?i.scrollParent[0].scrollLeft=s=i.scrollParent[0].scrollLeft+a.scrollSpeed:t.pageX-i.overflowOffset.left<a.scrollSensitivity&&(i.scrollParent[0].scrollLeft=s=i.scrollParent[0].scrollLeft-a.scrollSpeed))):(a.axis&&"x"===a.axis||(t.pageY-e(document).scrollTop()<a.scrollSensitivity?s=e(document).scrollTop(e(document).scrollTop()-a.scrollSpeed):e(window).height()-(t.pageY-e(document).scrollTop())<a.scrollSensitivity&&(s=e(document).scrollTop(e(document).scrollTop()+a.scrollSpeed))),a.axis&&"y"===a.axis||(t.pageX-e(document).scrollLeft()<a.scrollSensitivity?s=e(document).scrollLeft(e(document).scrollLeft()-a.scrollSpeed):e(window).width()-(t.pageX-e(document).scrollLeft())<a.scrollSensitivity&&(s=e(document).scrollLeft(e(document).scrollLeft()+a.scrollSpeed)))),s!==!1&&e.ui.ddmanager&&!a.dropBehaviour&&e.ui.ddmanager.prepareOffsets(i,t)}}),e.ui.plugin.add("draggable","snap",{start:function(){var t=e(this).data("ui-draggable"),i=t.options;t.snapElements=[],e(i.snap.constructor!==String?i.snap.items||":data(ui-draggable)":i.snap).each(function(){var i=e(this),a=i.offset();this!==t.element[0]&&t.snapElements.push({item:this,width:i.outerWidth(),height:i.outerHeight(),top:a.top,left:a.left})})},drag:function(t,i){var a,s,n,r,o,l,h,u,d,c,p=e(this).data("ui-draggable"),f=p.options,m=f.snapTolerance,g=i.offset.left,v=g+p.helperProportions.width,y=i.offset.top,b=y+p.helperProportions.height;for(d=p.snapElements.length-1;d>=0;d--)o=p.snapElements[d].left,l=o+p.snapElements[d].width,h=p.snapElements[d].top,u=h+p.snapElements[d].height,o-m>v||g>l+m||h-m>b||y>u+m||!e.contains(p.snapElements[d].item.ownerDocument,p.snapElements[d].item)?(p.snapElements[d].snapping&&p.options.snap.release&&p.options.snap.release.call(p.element,t,e.extend(p._uiHash(),{snapItem:p.snapElements[d].item})),p.snapElements[d].snapping=!1):("inner"!==f.snapMode&&(a=m>=Math.abs(h-b),s=m>=Math.abs(u-y),n=m>=Math.abs(o-v),r=m>=Math.abs(l-g),a&&(i.position.top=p._convertPositionTo("relative",{top:h-p.helperProportions.height,left:0}).top-p.margins.top),s&&(i.position.top=p._convertPositionTo("relative",{top:u,left:0}).top-p.margins.top),n&&(i.position.left=p._convertPositionTo("relative",{top:0,left:o-p.helperProportions.width}).left-p.margins.left),r&&(i.position.left=p._convertPositionTo("relative",{top:0,left:l}).left-p.margins.left)),c=a||s||n||r,"outer"!==f.snapMode&&(a=m>=Math.abs(h-y),s=m>=Math.abs(u-b),n=m>=Math.abs(o-g),r=m>=Math.abs(l-v),a&&(i.position.top=p._convertPositionTo("relative",{top:h,left:0}).top-p.margins.top),s&&(i.position.top=p._convertPositionTo("relative",{top:u-p.helperProportions.height,left:0}).top-p.margins.top),n&&(i.position.left=p._convertPositionTo("relative",{top:0,left:o}).left-p.margins.left),r&&(i.position.left=p._convertPositionTo("relative",{top:0,left:l-p.helperProportions.width}).left-p.margins.left)),!p.snapElements[d].snapping&&(a||s||n||r||c)&&p.options.snap.snap&&p.options.snap.snap.call(p.element,t,e.extend(p._uiHash(),{snapItem:p.snapElements[d].item})),p.snapElements[d].snapping=a||s||n||r||c)}}),e.ui.plugin.add("draggable","stack",{start:function(){var t,i=this.data("ui-draggable").options,a=e.makeArray(e(i.stack)).sort(function(t,i){return(parseInt(e(t).css("zIndex"),10)||0)-(parseInt(e(i).css("zIndex"),10)||0)});a.length&&(t=parseInt(e(a[0]).css("zIndex"),10)||0,e(a).each(function(i){e(this).css("zIndex",t+i)}),this.css("zIndex",t+a.length))}}),e.ui.plugin.add("draggable","zIndex",{start:function(t,i){var a=e(i.helper),s=e(this).data("ui-draggable").options;a.css("zIndex")&&(s._zIndex=a.css("zIndex")),a.css("zIndex",s.zIndex)},stop:function(t,i){var a=e(this).data("ui-draggable").options;a._zIndex&&e(i.helper).css("zIndex",a._zIndex)}})})(jQuery);

	// new YearsControlers();

	new ((function() {
		return function() {
			$(".tabs-handlers a")
				.each(function() {
					var $this = $(this);
					$this.append("<b>"+ $this.find("span").html() +"</b>");
				});
		};
	})());

	new ((function() {
		return function() {
			var items = $("#other-vacancies.content-item.hot-vacancies").find(".col322");
			var len = items.length;
			var prevLine = -1;
			var prevSum = 0;

			var setHei = function(line, sum) {
				var id = line * 3;
				items.eq(id + 0).outerHeight(sum);
				items.eq(id + 1).outerHeight(sum);
				items.eq(id + 2).outerHeight(sum);
			};

			items.each(function(i, item) {
				var line = parseInt(i / 3);
				if (line !== prevLine) {
					prevSum = $(item).outerHeight();
					// console.log( prevSum );
					// console.log( item );
					prevLine = line;
				}
				if (prevSum < $(item).outerHeight()) {
					prevSum = $(item).outerHeight();
				}
				if ( i % 3 == 2) {
					setHei(prevLine, prevSum);
				}
				// console.log( Math.ceil(i / 3) );
			});
			// setHei(prevLine, prevSum);

			items.eq(len - 1).addClass("_last");
			items.eq(len - 2).addClass("_last");
			items.eq(len - 3).addClass("_last");
		};
	})());

	new ((function() {
		return function() {

		$(function() {

			var items = [
				['.eventslist .event-title'],
				['.releasies-title'],
				['.eventsbefore .event-title']
			];

			for (var i = 0, l = items.length; i < l; i++) {
				var titles = $(items[i].join(','));
				var max = 0;
				titles
					.each(function() {
						if ($(this).height() > max) {
							max = $(this).height();
						}
					})
					.height(max);
			}

			});

		};
	})());

	new ((function() {
		return function() {
		$(function() {
			var items = $('ul.releasies > li');
			var max = 0;
			items
				.each(function() {
					if ($(this).height() > max) {
						max = $(this).height();
					}
				})
				.find('>a').attr('do', 'no').height(max + 78);
		});
		};
	})());

	/* jQuery Form Styler v1.4.6 | (c) Dimox | https://github.com/Dimox/jQueryFormStyler */
	(function(c){c.fn.styler=function(d){d=c.extend({idSuffix:"-styler",filePlaceholder:"\u0424\u0430\u0439\u043b \u043d\u0435 \u0432\u044b\u0431\u0440\u0430\u043d",fileBrowse:"\u041e\u0431\u0437\u043e\u0440...",selectSearch:!0,selectSearchLimit:10,selectSearchNotFound:"\u0421\u043e\u0432\u043f\u0430\u0434\u0435\u043d\u0438\u0439 \u043d\u0435 \u043d\u0430\u0439\u0434\u0435\u043d\u043e",selectSearchPlaceholder:"\u041f\u043e\u0438\u0441\u043a...",selectVisibleOptions:0,singleSelectzIndex:"100",selectSmartPositioning:!0},
	d);return this.each(function(){function q(){var c="",u="",b="",t="";void 0!==a.attr("id")&&""!=a.attr("id")&&(c=' id="'+a.attr("id")+d.idSuffix+'"');void 0!==a.attr("title")&&""!=a.attr("title")&&(u=' title="'+a.attr("title")+'"');void 0!==a.attr("class")&&""!=a.attr("class")&&(b=" "+a.attr("class"));var q=a.data(),g;for(g in q)""!=q[g]&&(t+=" data-"+g+'="'+q[g]+'"');this.id=c+t;this.title=u;this.classes=b}var a=c(this);a.is(":checkbox")?a.each(function(){if(1>a.parent("div.jq-checkbox").length){var d=
	function(){var d=new q,b=c("<div"+d.id+' class="jq-checkbox'+d.classes+'"'+d.title+'><div class="jq-checkbox__div"></div></div>');a.css({position:"absolute",zIndex:"-1",opacity:0,margin:0,padding:0}).after(b).prependTo(b);b.attr("unselectable","on").css({"-webkit-user-select":"none","-moz-user-select":"none","-ms-user-select":"none","-o-user-select":"none","user-select":"none",display:"inline-block",position:"relative",overflow:"hidden"});a.is(":checked")&&b.addClass("checked");a.is(":disabled")&&
	b.addClass("disabled");b.click(function(){b.is(".disabled")||(a.is(":checked")?(a.prop("checked",!1),b.removeClass("checked")):(a.prop("checked",!0),b.addClass("checked")),a.change());return!1});a.closest("label").add('label[for="'+a.attr("id")+'"]').click(function(a){b.click();a.preventDefault()});a.change(function(){a.is(":checked")?b.addClass("checked"):b.removeClass("checked")}).keydown(function(a){13!=a.which&&32!=a.which||b.click()}).focus(function(){b.is(".disabled")||b.addClass("focused")}).blur(function(){b.removeClass("focused")})};
	d();a.on("refresh",function(){a.parent().before(a).remove();d()})}}):a.is(":radio")?a.each(function(){if(1>a.parent("div.jq-radio").length){var d=function(){var d=new q,b=c("<div"+d.id+' class="jq-radio'+d.classes+'"'+d.title+'><div class="jq-radio__div"></div></div>');a.css({position:"absolute",zIndex:"-1",opacity:0,margin:0,padding:0}).after(b).prependTo(b);b.attr("unselectable","on").css({"-webkit-user-select":"none","-moz-user-select":"none","-ms-user-select":"none","-o-user-select":"none","user-select":"none",
	display:"inline-block",position:"relative"});a.is(":checked")&&b.addClass("checked");a.is(":disabled")&&b.addClass("disabled");b.click(function(){b.is(".disabled")||(b.closest("form").find('input[name="'+a.attr("name")+'"]').prop("checked",!1).parent().removeClass("checked"),a.prop("checked",!0).parent().addClass("checked"),a.change());return!1});a.closest("label").add('label[for="'+a.attr("id")+'"]').click(function(a){b.click();a.preventDefault()});a.change(function(){a.parent().addClass("checked")}).focus(function(){b.is(".disabled")||
	b.addClass("focused")}).blur(function(){b.removeClass("focused")})};d();a.on("refresh",function(){a.parent().before(a).remove();d()})}}):a.is(":file")?a.css({position:"absolute",top:0,right:0,width:"100%",height:"100%",opacity:0,margin:0,padding:0}).each(function(){if(1>a.parent("div.jq-file").length){var r=function(){var u=new q,b=c("<div"+u.id+' class="jq-file'+u.classes+'"'+u.title+' style="display: inline-block; position: relative; overflow: hidden"></div>'),t=c('<div class="jq-file__name">'+
	d.filePlaceholder+"</div>").appendTo(b);c('<div class="jq-file__browse">'+d.fileBrowse+"</div>").appendTo(b);a.after(b);b.append(a);a.is(":disabled")&&b.addClass("disabled");a.change(function(){t.text(a.val().replace(/.+[\\\/]/,""))}).focus(function(){b.addClass("focused")}).blur(function(){b.removeClass("focused")}).click(function(){b.removeClass("focused")})};r();a.on("refresh",function(){a.parent().before(a).remove();r()})}}):a.is("select")?a.each(function(){if(1>a.next("div.jqselect").length){var r=
	function(){function u(a){a.unbind("mousewheel DOMMouseScroll").bind("mousewheel DOMMouseScroll",function(a){var b=null;"mousewheel"==a.type?b=-1*a.originalEvent.wheelDelta:"DOMMouseScroll"==a.type&&(b=40*a.originalEvent.detail);b&&(a.preventDefault(),c(this).scrollTop(b+c(this).scrollTop()))})}function b(){i=0;for(len=g.length;i<len;i++){var a="",c="",b=a="",d="",k="";g.eq(i).prop("selected")&&(c="selected sel");g.eq(i).is(":disabled")&&(c="disabled");g.eq(i).is(":selected:disabled")&&(c="selected sel disabled");
	void 0!==g.eq(i).attr("class")&&(b=" "+g.eq(i).attr("class"),k=' data-jqfs-class="'+g.eq(i).attr("class")+'"');var f=g.eq(i).data(),l;for(l in f)""!=f[l]&&(a+=" data-"+l+'="'+f[l]+'"');a="<li"+k+a+' class="'+c+b+'">'+g.eq(i).text()+"</li>";g.eq(i).parent().is("optgroup")&&(void 0!==g.eq(i).parent().attr("class")&&(d=" "+g.eq(i).parent().attr("class")),a="<li"+k+' class="'+c+b+" option"+d+'">'+g.eq(i).text()+"</li>",g.eq(i).is(":first-child")&&(a='<li class="optgroup'+d+'">'+g.eq(i).parent().attr("label")+
	"</li>"+a));w+=a}}function t(){var n=new q,e=c("<div"+n.id+' class="jq-selectbox jqselect'+n.classes+'" style="display: inline-block; position: relative; z-index:'+d.singleSelectzIndex+'"><div class="jq-selectbox__select"'+n.title+'><div class="jq-selectbox__select-text"></div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div></div>');a.css({margin:0,padding:0}).wrap('<div class="jq-selectbox-wrapper" style="display: inline-block; position: relative;"></div>').after(e);
	var n=a.clone().appendTo("body"),p=n.width();n.remove();p!=a.width()&&a.parent().css("display","block");var n=c("div.jq-selectbox__select",e),h=c("div.jq-selectbox__select-text",e),p=g.filter(":selected");p.length?h.text(p.text()):h.text(g.first().text());b();var k="";d.selectSearch&&(k='<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="'+d.selectSearchPlaceholder+'"></div><div class="jq-selectbox__not-found">'+d.selectSearchNotFound+"</div>");var f=c('<div class="jq-selectbox__dropdown" style="position: absolute">'+
	k+'<ul style="position: relative; list-style: none; overflow: auto; overflow-x: hidden">'+w+"</ul></div>");e.append(f);var l=c("ul",f),m=c("li",f),v=c("input",f),r=c("div.jq-selectbox__not-found",f).hide();m.length<d.selectSearchLimit&&v.parent().hide();var x=0;m.each(function(){c(this).css({display:"inline-block","white-space":"nowrap"});c(this).width()>x&&(x=c(this).width());c(this).css({display:"block","white-space":"normal"})});e.width(a.outerWidth());x>f.width()&&f.width(x+f.width()-m.width());
	a.css({position:"absolute",opacity:0,height:e.outerHeight()});var t=e.outerHeight(),z=v.outerHeight(),y=l.css("max-height"),k=m.filter(".selected");1>k.length&&m.first().addClass("selected sel");var s=m.outerHeight(),A=f.css("top");"auto"==f.css("left")&&f.css({left:0});"auto"==f.css("top")&&f.css({top:t});f.hide();k.length&&(g.first().text()!=p.text()&&e.addClass("changed"),e.data("jqfs-class",k.data("jqfs-class")),e.addClass(k.data("jqfs-class")));if(a.is(":disabled"))return e.addClass("disabled"),
	!1;n.click(function(){a.focus();if(!navigator.userAgent.match(/(iPad|iPhone|iPod)/g)){if(d.selectSmartPositioning){var b=c(window),g=e.offset().top,k=b.height()-t-(g-b.scrollTop()),n=d.selectVisibleOptions,h=5*s,p=s*n;0<n&&6>n&&(h=p);if(k>h+z+20)f.height("auto").css({bottom:"auto",top:A}),h=function(){l.css("max-height",Math.floor((k-20-z)/s)*s)},h(),0<n&&6>n?l.css("max-height",p):6<n?(l.css("max-height",p),k<f.outerHeight()+20&&h()):"none"!=y&&(l.css("max-height",y),k<f.outerHeight()+20&&h());else{f.height("auto").css({top:"auto",
	bottom:A});var q=function(){l.css("max-height",Math.floor((g-b.scrollTop()-20-z)/s)*s)},h=function(){g-b.scrollTop()-20<f.outerHeight()+20&&q()};q();0<n&&6>n?l.css("max-height",p):6<n?(l.css("max-height",p),h()):"none"!=y&&(l.css("max-height",y),h())}}c("div.jqselect").css({zIndex:d.singleSelectzIndex-1}).removeClass("opened focused");e.css({zIndex:d.singleSelectzIndex});f.is(":hidden")?(c("div.jq-selectbox__dropdown:visible").hide(),f.show(),e.addClass("opened")):(f.hide(),e.removeClass("opened"));
	m.filter(".selected").length&&(0!=l.innerHeight()/s%2&&(s/=2),l.scrollTop(l.scrollTop()+m.filter(".selected").position().top-l.innerHeight()/2+s));v.length&&(v.val("").keyup(),r.hide(),v.focus().keyup(function(){var a=c(this).val();m.each(function(){c(this).html().match(RegExp(".*?"+a+".*?","i"))?c(this).show():c(this).hide()});1>m.filter(":visible").length?r.show():r.hide()}));u(l);return!1}});m.hover(function(){c(this).siblings().removeClass("selected")});var B=m.filter(".selected").text();m.filter(".selected").text();
	m.filter(":not(.disabled):not(.optgroup)").click(function(){var b=c(this),l=b.text();if(B!=l){var m=b.index();b.is(".option")&&(m-=b.prevAll(".optgroup").length);b.addClass("selected sel").siblings().removeClass("selected sel");g.prop("selected",!1).eq(m).prop("selected",!0);B=l;h.text(l);g.first().text()!=l?e.addClass("changed"):e.removeClass("changed");e.data("jqfs-class")&&e.removeClass(e.data("jqfs-class"));e.data("jqfs-class",b.data("jqfs-class"));e.addClass(b.data("jqfs-class"));a.change()}v.length&&
	(v.val("").keyup(),r.hide());f.hide();e.removeClass("opened")});f.mouseout(function(){c("li.sel",f).addClass("selected")});a.change(function(){h.text(g.filter(":selected").text());m.removeClass("selected sel").not(".optgroup").eq(a[0].selectedIndex).addClass("selected sel")}).focus(function(){e.addClass("focused")}).blur(function(){e.removeClass("focused")}).bind("keydown keyup",function(b){h.text(g.filter(":selected").text());m.removeClass("selected sel").not(".optgroup").eq(a[0].selectedIndex).addClass("selected sel");
	38!=b.which&&37!=b.which&&33!=b.which||f.scrollTop(f.scrollTop()+m.filter(".selected").position().top);40!=b.which&&39!=b.which&&34!=b.which||f.scrollTop(f.scrollTop()+m.filter(".selected").position().top-f.innerHeight()+s);13==b.which&&f.hide()});c(document).on("click",function(a){c(a.target).parents().hasClass("jq-selectbox")||"OPTION"==a.target.nodeName||(v.length&&v.val("").keyup(),f.hide().find("li.sel").addClass("selected"),e.removeClass("focused opened"))})}function r(){var d=new q,e=c("<div"+
	d.id+' class="jq-select-multiple jqselect'+d.classes+'"'+d.title+' style="display: inline-block; position: relative"></div>');a.css({margin:0,padding:0}).wrap('<div class="jq-selectbox-wrapper" style="display: inline-block; position: relative;"></div>').after(e);var d=a.clone().appendTo("body"),p=d.width();d.remove();p!=a.width()&&a.parent().css("display","block");b();e.append('<ul style="position: relative">'+w+"</ul>");var h=c("ul",e).css({"overflow-x":"hidden"}),k=c("li",e).attr("unselectable",
	"on").css({"-webkit-user-select":"none","-moz-user-select":"none","-ms-user-select":"none","-o-user-select":"none","user-select":"none","white-space":"nowrap"}),d=a.attr("size"),p=h.outerHeight(),f=k.outerHeight();void 0!==d&&0<d?h.css({height:f*d}):h.css({height:4*f});p>e.height()&&(h.css("overflowY","scroll"),u(h),k.filter(".selected").length&&h.scrollTop(h.scrollTop()+k.filter(".selected").position().top));a.is(":disabled")?(e.addClass("disabled"),g.each(function(){c(this).is(":selected")&&k.eq(c(this).index()).addClass("selected")}),
	e.width(a.outerWidth()),e.width(e.width()-(e.outerWidth()-e.width())),a.css({position:"absolute",opacity:0,height:e.outerHeight()})):(e.width(a.outerWidth()),e.width(e.width()-(e.outerWidth()-e.width())),a.css({position:"absolute",opacity:0,height:e.outerHeight()}),k.filter(":not(.disabled):not(.optgroup)").click(function(b){a.focus();e.removeClass("focused");var d=c(this);b.ctrlKey||b.metaKey||d.addClass("selected");b.shiftKey||d.addClass("first");b.ctrlKey||(b.metaKey||b.shiftKey)||d.siblings().removeClass("selected first");
	if(b.ctrlKey||b.metaKey)d.is(".selected")?d.removeClass("selected first"):d.addClass("selected first"),d.siblings().removeClass("first");if(b.shiftKey){var f=!1,h=!1;d.siblings().removeClass("selected").siblings(".first").addClass("selected");d.prevAll().each(function(){c(this).is(".first")&&(f=!0)});d.nextAll().each(function(){c(this).is(".first")&&(h=!0)});f&&d.prevAll().each(function(){if(c(this).is(".selected"))return!1;c(this).not(".disabled, .optgroup").addClass("selected")});h&&d.nextAll().each(function(){if(c(this).is(".selected"))return!1;
	c(this).not(".disabled, .optgroup").addClass("selected")});1==k.filter(".selected").length&&d.addClass("first")}g.prop("selected",!1);k.filter(".selected").each(function(){var a=c(this),b=a.index();a.is(".option")&&(b-=a.prevAll(".optgroup").length);g.eq(b).prop("selected",!0)});a.change()}),g.each(function(a){c(this).data("optionIndex",a)}),a.change(function(){k.removeClass("selected");var a=[];g.filter(":selected").each(function(){a.push(c(this).data("optionIndex"))});k.not(".optgroup").filter(function(b){return-1<
	c.inArray(b,a)}).addClass("selected")}).focus(function(){e.addClass("focused")}).blur(function(){e.removeClass("focused")}),p>e.height()&&a.keydown(function(a){38!=a.which&&37!=a.which&&33!=a.which||h.scrollTop(h.scrollTop()+k.filter(".selected").position().top-f);40!=a.which&&39!=a.which&&34!=a.which||h.scrollTop(h.scrollTop()+k.filter(".selected:last").position().top-h.innerHeight()+2*f)}))}var g=c("option",a),w="";a.is("[multiple]")?r():t()};r();a.on("refresh",function(){a.parent().before(a).remove();
	r()});a.on("adaptiveWidth",function(){a.css({position:"static"});a.next().width(a.outerWidth());a.css({position:"absolute"})});c(window).on("resize",function(){a.trigger("adaptiveWidth")})}}):a.is(":reset")&&a.click(function(){setTimeout(function(){a.closest("form").find("input, select").trigger("refresh")},1)})})}})(jQuery);

	new ((function() {
		return function() {
			var parent = $("._send-summary");
			parent.find("input[type=file], .agree-with-terms input").styler({
				filePlaceholder: "Загрузить резюме",
				fileBrowse: "Выбрать файл"
			});

			var addSoc = $(".add-soc-input");
			var echelon = "<div class='input-wrap'><input type='text' placeholder='Добавьте ваш профиль в социальной сети'></div>";
			addSoc.on("click", function() {
				addSoc.before(echelon).prev().hide().slideDown(300).find("input").focus();
				return false;
			});
		};
	})());

});
